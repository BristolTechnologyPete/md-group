<?php


namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use App\Entity\ExchangeRates;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\Exception\TransportException;
use Symfony\Component\Routing\Annotation\Route as BaseRoute;

/**
 * ExchangeRate controller.
 * @BaseRoute("/api", name="api_")
 */
class ExceptionController extends AbstractFOSRestController
{

    /**
     * Handles any invalid routes.
     *
     * @return JsonResponse
     */
    public function errorAction()
    {

        $result = [
            'error' => 1,
            'msg' => 'Invalid Request'
        ];

        return new JsonResponse($result, 500);
    }
}
