<?php


namespace App\Controller;

use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use App\Entity\ExchangeRates;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\Exception\TransportException;
use Symfony\Component\Routing\Annotation\Route as BaseRoute;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * ExchangeRate controller.
 * @BaseRoute("/api", name="api_")
 */
class ExchangeRateController extends AbstractFOSRestController
{
    protected $rates;

    /**
     * The validation on here is default so it wil error when invalid valies are used
     * but it will not return the error message defined in the specification at this stage.
     * Get Exchange Value.
     * @Rest\Get("/exchange/{amount}/{src}/{dst}", requirements={"amount" = "\d+","src" = "(GBP|USD|CAD|JPY|RUR|HKD|CHF|EUR|gbp|usd|cad|jpy|rur|hkd|chf|eur)","dst" = "(GBP|USD|CAD|JPY|RUR|HKD|CHF|EUR|gbp|usd|cad|jpy|rur|hkd|chf|eur)"})
     *
     * @return JsonResponse
     */
    public function getExchangeRateAction($amount, $src, $dst)
    {
        try {
            list($converted, $cached) = $this->getExchangeValue(strtoupper($src), strtoupper($dst), $amount);
        } catch (Exception $e) {
            $result = [
                'error' => $e->getMessage()
            ];

            return new JsonResponse($result, $e->getCode());
        }

        $result = [
            'error' => 0,
            'amount' => $converted,
            'fromCache' => $cached ? 1 : 0
        ];

        return new JsonResponse($result, 200);
    }

    /**
     * @return mixed
     */
    public function getRates()
    {
        return $this->rates;
    }

    /**
     * @param mixed $rates
     */
    public function setRates($rates): void
    {
        $this->rates = json_decode($rates);
    }

    /**
     * @param mixed $rates
     */
    /**
     * @param $src source currency symbol
     * @param $dst destination currency symbol
     * @param $value amount in source currency
     * @return array 2dp formatted amount in destination currency and bool of cache status
     */
    public function getExchangeValue($src, $dst, $value): array
    {
        /** @var bool $cached */
        $cached = $this->fetchRates();

        $base = $this->getRates()->base;
        if ($src === $base) {
            $sourceRate = 1;
        } else {
            $sourceRate = $this->getRates()->rates->{$src};
        }

        if ($dst === $base) {
            $dstRate = 1;
        } else {
            $dstRate = $this->getRates()->rates->{$dst};
        }

        // TODO check for / zero
        $amtInBase = $value / $sourceRate;
        $amtInDest = $amtInBase * $dstRate;
        $converted = number_format((float)($amtInDest), 2, '.', '');

        return [$converted, $cached];
    }

    /**
     * Display info about the API on a get request to this endpoint.
     * @Rest\Get("/exchange/info")
     *
     * @return JsonResponse
     */
    public function getExchangeInfo()
    {
        return new JsonResponse([
            'error' => 0,
            'msg' => 'API written by Pete Siviter'
        ], 200);
    }

    /**
     * Display info about the API.
     * @Rest\Get("/cache/clear")
     *
     * @return JsonResponse
     */
    public function getCacheClear()
    {
        try {
            $this->clearExchangeRates();
        } catch (Exception $e) {
            $result = [
                'error' => $e->getMessage()
            ];

            return new JsonResponse($result, 500);
        }

        $result = [
            'error' => 0,
            'msg' => 'Ok'
        ];

        return new JsonResponse($result, 200);
    }

    /**
     * Load the rates either from the cache or the api of the cache is stale or empty (first run)
     * @return bool|JsonResponse
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws Exception
     */
    protected function fetchRates()
    {
        $cached = true;
        $rates = $this->loadExchangeRates();
        if (empty($rates) || time() > $rates->getTimeOut()) {
            $this->clearExchangeRates();
            $cached = false;

            $httpClient = HttpClient::create();
            try {
                $response = $httpClient->request('GET', 'https://api.exchangeratesapi.io/latest');
                $statusCode = $response->getStatusCode();
            } catch (TransportException $e) {
                throw new Exception('Transport Error Communicating with exchangeratesapi', 500);
            }

            if ($statusCode !== 200) {
                throw new Exception('Unexpected Result Code Communicating with exchangeratesapi', 500);
            }

            try {
                $content = $response->getContent();
            } catch (Exception $e) {
                throw new Exception('Exception fetching content: ' . $e->getMessage(), 500);
            }

            // Place the fresh rates in the cache.
            $this->storeExchangeRates($content);
            $this->setRates($content);
        } else {
            $this->setRates($rates->getExchangeRateJson());
        }
        return $cached;
    }

    /**
     * Store ExchangeRates in memory only table in DB, personally I'd do it in redis as you can auto expire the
     * values you store and it's very performant.
     * @param $rates
     */
    public function storeExchangeRates($rates)
    {
        $em = $this->getDoctrine()->getManager();
        // Truncate the table as there can be only one...

        /** @var ExchangeRates $exchangeRates */
        $exchangeRates = new ExchangeRates();
        $exchangeRates->setExchangeRateJson($rates);
        $expiresTime = time() + $_ENV['EXPIRY_TIME'];
        $exchangeRates->setTimeOut($expiresTime);

        $em->persist($exchangeRates);
        $em->flush();
    }

    /**
     * Fetch ExchangeRates from the memory only table in DB
     *
     * @return \App\Entity\ExchangeRates
     */
    public function loadExchangeRates()
    {
        $em = $this->getDoctrine()->getManager();
        /** @var ExchangeRates $exchangeRates */
        $exchangeRates = $em->createQueryBuilder()->select('exch')
            ->from("App\Entity\ExchangeRates", 'exch')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        return $exchangeRates;
    }

    /**
     * This method clears the exchange rate table prior to loading the single record exchange rates.
     * There should be only one row in the db so this will be performant.
     *
     * @return bool
     * @throws Exception
     */
    public function clearExchangeRates()
    {
        $em = $this->getDoctrine()->getManager();
        $cmd = $em->getClassMetadata("App\Entity\ExchangeRates");
        $connection = $em->getConnection();
        $connection->beginTransaction();

        try {
            $connection->query('SET FOREIGN_KEY_CHECKS=0');
            $connection->query('TRUNCATE TABLE ' . $cmd->getTableName());
            $connection->query('SET FOREIGN_KEY_CHECKS=1');
            $connection->commit();
            $em->flush();
        } catch (\Exception $e) {
            throw $e;
        }

        return true;
    }
}
