<?php
namespace App\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\ExchangeRates;

class ExchangeRateType extends AbstractType
{

    /**
     * This class was used to perform validations but I simplified the process.  This could still be re-enabled
     * to perform more sophisticated validations if needed.
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('source_currency')
            ->add('dest_currency')
            ->add('rate')
            ->add('save', SubmitType::class)
        ;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ExchangeRates::class,
            'csrf_protection' => false
        ));
    }
}
