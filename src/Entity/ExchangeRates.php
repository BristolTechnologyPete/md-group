<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * This is the exchange rate table persisted to the DB
 * @ORM\Entity
 * @ORM\Table(name="exchange_rates", options={"engine"="memory"})
 */
class ExchangeRates
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=4096)
     * @Assert\NotBlank()
     */
    private $exchangeRateJson;

    /**
     * @ORM\Column(type="integer")
     */
    private $timeOut;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getExchangeRateJson()
    {
        return $this->exchangeRateJson;
    }

    /**
     * @param mixed $exchangeRateJson
     */
    public function setExchangeRateJson($exchangeRateJson)
    {
        $this->exchangeRateJson = $exchangeRateJson;
    }

    /**
     * @return mixed
     */
    public function getTimeOut()
    {
        return $this->timeOut;
    }

    /**
     * @param mixed $timeOut
     */
    public function setTimeOut($timeOut): void
    {
        $this->timeOut = $timeOut;
    }

}
