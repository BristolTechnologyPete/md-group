<?php

namespace App\DataFixtures;

use App\Entity\ExchangeRates;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    private $currencies = ['GBP', 'USD', 'EUR'];

    public function load(ObjectManager $manager)
    {
        foreach ($this->currencies as $sourceCurrency) {
            foreach ($this->currencies as $destCurrency) {
                $exchangeRate = new ExchangeRates();
                $exchangeRate->setExchangeRateJson($sourceCurrency);
                $exchangeRate->setDestCurrency($destCurrency);
                $exchangeRate->setRate(0);
                $manager->persist($exchangeRate);
            }
        }

        $manager->flush();
    }
}
