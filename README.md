Introduction
============

This is an implementation of the test as requested.  I used Symfony 4 with the FOSRestBundle for the implementation (can't use Symfony 5 yet as it's currently incompatible with the FOSRestBundle).  I have tested the results are accurate in a "line of sight" way, so "in theory" the exchange rates are good and tested qualitatively, i.e. if I convert USD to GBP it should be a smaller number, roughly two thirds of the amount etc.

I also didn't need to implement the inverse function though I added it to the model it's not used anywhere because the caching mechanism makes a single request for all rates and exchanges from the cache in any direction until the cached values expire and it refreshes from exchangeratesapi.  The expirty time is set in the .env file.

It is using a memory table from mariadb (or mysql) for the cache.  I would have preferred to use redis probably but it would have required some setting up at your end, so I left it as mysql.

I used the annotation methodology of Symfony to carry out validation.

I have defaulted to get methods for all requests as none were specified.

Running: -
=======

I have left out the vendors dir from the repo, so you should run composer install to install all the deps (including Symfony), include the --dev option to ensure the web-server-bundle is installed to be able to use the built in web server.

Go to web root you want to install into: -

cd <web root>

git clone https://BristolTechnologyPete@bitbucket.org/BristolTechnologyPete/md-group.git

cd md-group

composer install --dev

Then start the server on port 8000 with

bin/console server:run

Using: -
=====

You should then be able to make requests in postman with e.g.: -


-------------------------

localhost:8000/api/exchange/3/EUR/USD

With a response similar to: -
{
    "error": 0,
    "amount": "3.34",
    "fromCache": 0
}

The error is either 0 or >= 500 for an error condition fromCache is 1 or 0 if the exchange calculation was managed from the cache.

-------------------------

localhost:8000/api/exchange/info

Will respond: -

{
    "error": 0,
    "msg": "API written by Pete Siviter"
}

-------------------------

localhost:8000/api/cache/clear

Will use a server specific truncate table command to clear the rates table (intentional) and return: -
{
    "error": 0,
    "msg": "Ok"
}
